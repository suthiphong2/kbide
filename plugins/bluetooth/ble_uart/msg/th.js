Blockly.Msg.BLE_UART_ON_DATA_TITLE = "เมื่อมีข้อมูล BLE UART";
Blockly.Msg.BLE_UART_ON_DATA_TOOLTIP = "เมื่อมีข้อมูล BLE UART";
Blockly.Msg.BLE_UART_ON_DATA_HELPURL = "";

Blockly.Msg.BLE_UART_READ_TEXT_TITLE = "อ่านตัวอักษร BLE UART";
Blockly.Msg.BLE_UART_READ_TEXT_TOOLTIP = "อ่านตัวอักษร BLE UART";
Blockly.Msg.BLE_UART_READ_TEXT_HELPURL = "";

Blockly.Msg.BLE_UART_SEND_TITLE = "ส่ง BLE UART";
Blockly.Msg.BLE_UART_SEND_TOOLTIP = "ส่ง BLE UART";
Blockly.Msg.BLE_UART_SEND_HELPURL = "";

Blockly.Msg.BLE_UART_TEXT_NUMBER_TOOLTIP = "ตัวอักษรหรือตัวเลข";
Blockly.Msg.BLE_UART_TEXT_NUMBER_HELPURL = "";
